import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RestauranteComponent } from './restaurante/restaurante.component';
import { routing } from './app.routing';
import { PratosComponent } from './pratos/pratos.component';
import { RestauranteCadastrarComponent } from './restaurante/restaurante-cadastrar/restaurante-cadastrar.component';
import { RestauranteEditarComponent } from './restaurante/restaurante-editar/restaurante-editar.component';
import { RestauranteService } from './shared/restaurante.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RestauranteComponent,
    PratosComponent,
    RestauranteCadastrarComponent,
    RestauranteEditarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [RestauranteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
