import { ModuleWithProviders } from '@angular/core';
import { RestauranteComponent } from './restaurante/restaurante.component';
import { RestauranteCadastrarComponent } from './restaurante/restaurante-cadastrar/restaurante-cadastrar.component';
import { RestauranteEditarComponent } from './restaurante/restaurante-editar/restaurante-editar.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router'



const  APP_ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'restaurante', component: RestauranteComponent},
    {path: 'restaurante/restaurante-cadastrar', component: RestauranteCadastrarComponent},
    {path: 'restaurante/restaurante-editar/:id', component: RestauranteEditarComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);