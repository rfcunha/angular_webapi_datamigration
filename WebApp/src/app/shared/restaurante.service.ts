import { Injectable } from '@angular/core';
import { Restaurante } from './restaurante.model';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs'

@Injectable({
    providedIn: 'root'
})

export class RestauranteService {

    formData:Restaurante;
    readonly rootUrl = 'http://localhost/Restaurante.WebApi/api';
    list: Restaurante[];


    constructor(private http:HttpClient){

    }

    createRestaurante(formData: Restaurante){

        return this.http.post(this.rootUrl+'/Restaurante/', formData);

    }

    getRestauranteById(id: number): Observable<Restaurante>{
        //console.log(this.http.get(this.rootUrl + '/Restaurante/' + id));
        return this.http.get<Restaurante>(this.rootUrl + '/Restaurante/' + id);
    }

    updateRestaurante(formData: Restaurante) {
        console.log(formData);
        return this.http.put(this.rootUrl + '/Restaurante/' + formData.res_id, formData);
      }
    
      deleteRestaurante(id: number) {
        return this.http.delete(this.rootUrl + 'Restaurante/' + id);
      }

    // getListRestaurante(){
        
    //     return this.http.get(this.rootUrl+'/Restaurante')
    //     .toPromise()
    //     .then(res => this.list = res as Restaurante[]);

    // }
}
