import { Component, OnInit } from '@angular/core';
import { RestauranteService } from 'src/app/shared/restaurante.service';
import { Restaurante } from 'src/app/shared/restaurante.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {first} from "rxjs/operators";
import { forbiddenNameValidator } from 'src/app/shared/forbidden-name.directive';


@Component({
  selector: 'app-restaurante-editar',
  templateUrl: './restaurante-editar.component.html',
  styleUrls: ['./restaurante-editar.component.css']
})
export class RestauranteEditarComponent implements OnInit {

  submitted: boolean = false;
  isLoadingResults: boolean = false;
  restaurante: Restaurante;
  editFormGroup: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: RestauranteService
    ) {

      //this.createForm();

     }

  ngOnInit() {

    const id = this.route.snapshot.params['id'];
    this.ObterRestaurante(id);

    // this.editFormGroup = new FormGroup({
    //   'res_nome': new FormControl(this.restaurante.res_nome, [
    //     Validators.required,
    //     Validators.minLength(4),
    //     forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
    //   ]),
    // });

    // this.editFormGroup = this.fb.group({
    //    'res_nome': [this.restaurante.res_nome, [
    //        Validators.required,
    //        Validators.minLength(4),
    //        Validators.maxLength(100),
    //        forbiddenNameValidator(/bob/i)
    //      ]
    //    ],
    //  });
  }

  private ObterRestaurante(id:number)
  {
    if(id === 0)
    {
      this.restaurante = {
        res_id: null,
        res_nome: null,
        res_status: null,
        res_dt_cadastro:null
      };

      this.isLoadingResults = false;
    }
    else{      
      this.service.getRestauranteById(id).subscribe( data => { this.restaurante = Object.assign({},data); });
      this.isLoadingResults = true;
    }
  }

  onSubmit(editForm:NgForm) {

    this.submitted = true;


  alert('SUCCESS!! :-)')

    console.log(editForm);

    //  this.service.updateRestaurante(editForm.value).subscribe(
    //      data => {
    //        this.router.navigate(['restaurante']);
    //      },
    //      error => {
    //        alert(error);
    //      });
  }



  resetForm(form?: NgForm)
  {
    if(form != null)
    {
      form.resetForm();

      // this.service.formData = {

      //   res_id:0,
      //   res_nome: '',
      //   res_dt_cadastro: new Date(),
      //   res_status: true
      // }
    }
  }

}
