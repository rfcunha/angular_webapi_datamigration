import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestauranteCadastrarComponent } from './restaurante-cadastrar.component';

describe('RestauranteCadastrarComponent', () => {
  let component: RestauranteCadastrarComponent;
  let fixture: ComponentFixture<RestauranteCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestauranteCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestauranteCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
