import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Restaurante } from '../../shared/restaurante.model';
import { RestauranteService } from 'src/app/shared/restaurante.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-restaurante-cadastrar',
  templateUrl: './restaurante-cadastrar.component.html',
  styleUrls: ['./restaurante-cadastrar.component.css']
})
export class RestauranteCadastrarComponent implements OnInit {

  constructor(private service: RestauranteService, private toastr: ToastrService ) {     

  }

  ngOnInit() {
  }

  resetForm(createForm?: NgForm)
  {
    if(createForm != null)
    {
      createForm.resetForm();

      // this.service.formData = {
  
      //   res_id:0,
      //   res_nome: '',
      //   res_dt_cadastro: new Date(),
      //   res_status: true
      // }
    }
  }

  onSubmit(createForm:NgForm)
  {
    //console.log(createForm.value);

    createForm.value.res_status = true;
    createForm.value.res_dt_cadastro = new Date();

    this.service.createRestaurante(createForm.value).subscribe(
      res=> {
        this.resetForm(createForm);
        this.toastr.success('Restaurante salvo com sucesso','Sucesso');

      },
      err => {
        console.log(err);
      }
    )
  }

}
