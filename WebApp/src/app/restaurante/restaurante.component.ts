import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http'
//import { RestauranteService } from 'src/app/shared/restaurante.service';
import { Restaurante } from '../shared/restaurante.model';

@Component({
  selector: 'app-restaurante',
  templateUrl: './restaurante.component.html',
  styleUrls: ['./restaurante.component.css']
})

export class RestauranteComponent implements OnInit {

  readonly rootUrl = 'http://localhost/Restaurante.WebApi/api';
  list: Restaurante[];

  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.getListRestaurante();
  }

  private getListRestaurante(){
        
    return this.http.get(this.rootUrl+'/Restaurante')
    .toPromise()
    .then(res => this.list = res as Restaurante[]);

}

}
