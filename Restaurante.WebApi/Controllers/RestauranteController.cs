﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Restaurante.Dal;

namespace Restaurante.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestauranteController : ControllerBase
    {
        private readonly RestauranteContext _context;

        public RestauranteController(RestauranteContext context)
        {
            _context = context;
        }

        // GET: api/Restaurante
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dal.Restaurante>>> GetRestaurante()
        {
            return await _context.Restaurante.ToListAsync();
        }

        // GET: api/Restaurante/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dal.Restaurante>> GetRestaurante(int id)
        {
            var restaurante = await _context.Restaurante.FindAsync(id);

            if (restaurante == null)
            {
                return NotFound();
            }

            return restaurante;
        }

        // PUT: api/Restaurante/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestaurante(int id, [FromBody]Dal.Restaurante restaurante)
        {
            if (id != restaurante.res_id)
            {
                return BadRequest();
            }

            _context.Entry(restaurante).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestauranteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Restaurante
        [HttpPost]
        public async Task<ActionResult<Dal.Restaurante>> PostRestaurante(Dal.Restaurante restaurante)
        {
            _context.Restaurante.Add(restaurante);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRestaurante", new { id = restaurante.res_id }, restaurante);
        }

        // DELETE: api/Restaurante/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Dal.Restaurante>> DeleteRestaurante(int id)
        {
            var restaurante = await _context.Restaurante.FindAsync(id);
            if (restaurante == null)
            {
                return NotFound();
            }

            _context.Restaurante.Remove(restaurante);
            await _context.SaveChangesAsync();

            return restaurante;
        }

        private bool RestauranteExists(int id)
        {
            return _context.Restaurante.Any(e => e.res_id == id);
        }
    }
}
