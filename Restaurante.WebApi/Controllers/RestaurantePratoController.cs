﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Restaurante.Dal;

namespace Restaurante.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantePratoController : ControllerBase
    {
        private readonly RestauranteContext _context;

        public RestaurantePratoController(RestauranteContext context)
        {
            _context = context;
        }

        // GET: api/RestaurantePratoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RestaurantePrato>>> GetRestaurantePrato()
        {
            return await _context.RestaurantePrato.ToListAsync();
        }

        // GET: api/RestaurantePratoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RestaurantePrato>> GetRestaurantePrato(int id)
        {
            var restaurantePrato = await _context.RestaurantePrato.FindAsync(id);

            if (restaurantePrato == null)
            {
                return NotFound();
            }

            return restaurantePrato;
        }

        // PUT: api/RestaurantePratoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestaurantePrato(int id, RestaurantePrato restaurantePrato)
        {
            if (id != restaurantePrato.rpr_id)
            {
                return BadRequest();
            }

            _context.Entry(restaurantePrato).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestaurantePratoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RestaurantePratoes
        [HttpPost]
        public async Task<ActionResult<RestaurantePrato>> PostRestaurantePrato(RestaurantePrato restaurantePrato)
        {
            _context.RestaurantePrato.Add(restaurantePrato);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRestaurantePrato", new { id = restaurantePrato.rpr_id }, restaurantePrato);
        }

        // DELETE: api/RestaurantePratoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RestaurantePrato>> DeleteRestaurantePrato(int id)
        {
            var restaurantePrato = await _context.RestaurantePrato.FindAsync(id);
            if (restaurantePrato == null)
            {
                return NotFound();
            }

            _context.RestaurantePrato.Remove(restaurantePrato);
            await _context.SaveChangesAsync();

            return restaurantePrato;
        }

        private bool RestaurantePratoExists(int id)
        {
            return _context.RestaurantePrato.Any(e => e.rpr_id == id);
        }
    }
}
