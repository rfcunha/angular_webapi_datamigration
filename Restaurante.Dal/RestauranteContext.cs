﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Restaurante.Dal
{
    public class RestauranteContext : DbContext
    {
        public DbSet<Restaurante> Restaurante { get; set; }
        public DbSet<RestaurantePrato> RestaurantePrato { get; set; }

        public RestauranteContext(DbContextOptions<RestauranteContext> options) : base(options)
        {

        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=.\\;Database=Restaurante;Trusted_Connection=True;MultipleActiveResultSets=true");
        //}
    }
}
