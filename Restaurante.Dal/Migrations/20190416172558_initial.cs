﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Restaurante.Dal.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Restaurante",
                columns: table => new
                {
                    res_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    res_nome = table.Column<string>(nullable: true),
                    res_status = table.Column<bool>(nullable: false),
                    res_dt_cadastro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurante", x => x.res_id);
                });

            migrationBuilder.CreateTable(
                name: "RestaurantePrato",
                columns: table => new
                {
                    rpr_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    rpr_nome = table.Column<string>(nullable: true),
                    rpr_descricao = table.Column<string>(nullable: true),
                    rpr_status = table.Column<bool>(nullable: false),
                    rpr_dt_cadastro = table.Column<DateTime>(nullable: false),
                    res_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RestaurantePrato", x => x.rpr_id);
                    table.ForeignKey(
                        name: "FK_RestaurantePrato_Restaurante_res_id",
                        column: x => x.res_id,
                        principalTable: "Restaurante",
                        principalColumn: "res_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RestaurantePrato_res_id",
                table: "RestaurantePrato",
                column: "res_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RestaurantePrato");

            migrationBuilder.DropTable(
                name: "Restaurante");
        }
    }
}
