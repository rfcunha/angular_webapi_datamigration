﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Restaurante.Dal
{
    public class Restaurante
    {
        [Key]
        public int res_id { get; set; }
        public string res_nome { get; set; }
        public bool res_status { get; set; }
        public DateTime res_dt_cadastro { get; set; }

        public ICollection<RestaurantePrato> RestaurantePrato { get; set; }
    }
}
