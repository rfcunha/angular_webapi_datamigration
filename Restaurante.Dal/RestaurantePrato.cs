﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Restaurante.Dal
{
    public class RestaurantePrato
    {
        [Key]
        public int rpr_id { get; set; }
        public string rpr_nome { get; set; }
        public string rpr_descricao { get; set; }
        public bool rpr_status { get; set; }
        public DateTime rpr_dt_cadastro { get; set; }
        public int res_id { get; set; }

        public virtual Restaurante Restaurante { get; set; }
    }
}
